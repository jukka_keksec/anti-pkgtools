Welcome to the repo for anti-pkgtools

These are slightly modified pkgtools for Antithesis based on the slackware pkgtools
and modified to work better with Antithesis Linux and the suckless 
userland as well as to integrate more fluidly into the antithesis 
containerizing system.

Everything here is WTFPL, thank you, have a nice day.
